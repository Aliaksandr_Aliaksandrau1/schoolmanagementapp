var mongoose = require('mongoose');

var HomeTaskSchema = new mongoose.Schema({
  date: Date,
  task: String,
  subject: {type: mongoose.Schema.Types.ObjectId, ref: 'Subject'},
  teacher: {type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  schoolClass: [{type: mongoose.Schema.Types.ObjectId, ref: 'User' }]
 });

mongoose.model('HomeTask', HomeTaskSchema);