var mongoose = require('mongoose');

var SubjectSchema = new mongoose.Schema({
  title: String,
  teachers: [{type: mongoose.Schema.Types.ObjectId, ref: 'User' }]  
 });

mongoose.model('Subject', SubjectSchema);