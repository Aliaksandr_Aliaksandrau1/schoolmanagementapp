var mongoose = require('mongoose');

var JournalRecordSchema = new mongoose.Schema({
  type: { mark: Number,
  		state: String},
  date: Date,
  comment: String,
  subject: {type: mongoose.Schema.Types.ObjectId, ref: 'Subject'},
  teacher: {type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  pupil: {type: mongoose.Schema.Types.ObjectId, ref: 'User' }
 });

mongoose.model('JournalRecord',JournalRecordSchema);