var mongoose = require('mongoose');

var LessonSchema = new mongoose.Schema({
  day: String,
  lessonNumber: Number,
  quarter: Number,
  subject: {type: mongoose.Schema.Types.ObjectId, ref: 'Subject'},
  teacher: {type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  schoolClass: [{type: mongoose.Schema.Types.ObjectId, ref: 'User' }]
 });

mongoose.model('Lesson', LessonSchema);