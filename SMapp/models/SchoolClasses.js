var mongoose = require('mongoose');

var SchoolClassSchema = new mongoose.Schema({
  number: Number,
  title: String,
  graduationDate: Date,
  pupils: [{type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
  scManager: {type: mongoose.Schema.Types.ObjectId, ref: 'User' }
 });

mongoose.model('SchoolClass', SchoolClassSchema);