var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

var HomeTask = mongoose.model('HomeTask');
var JournalRecord = mongoose.model('JournalRecord');
var Lesson = mongoose.model('Lesson');
var SchoolClass = mongoose.model('SchoolClass');
var Subject = mongoose.model('Subject');
var User = mongoose.model('User');

var passport = require('passport');
var jwt = require('express-jwt');

var auth = jwt({secret: 'SECRET', userProperty: 'payload'});




// subjects



// users


router.get('/pupils',  function(req, res, next) {

  console.log("test");

  User.find({"role":"pupil"},function(err, pupils){
    if(err){return next(err);}
    res.json(pupils);
  });
});



router.post('/user', function(req, res, next) {
  var user = new User(req.body);
  user.save(function(err, user){
    if(err){ return next(err); }

    res.json(user);
  });
});


router.get('/teachers', function(req, res, next) {
  User.find({"role":"teacher"}, function(err, teachers){
    if(err){return next(err);}
    res.json(teachers);
  });
});


// authentification

router.post('/register', function(req, res, next){


  if(!req.body.username || !req.body.password){
    return res.status(400).json({message: 'Please fill out all fields'});
  }


  var user = new User();
  user.username = req.body.username;
  user.role = req.body.role;
  user.active = req.body.active;
  user.personInfo = req.body.personInfo;
  user.contactInfo = req.body.contactInfo;

  user.setPassword(req.body.password)

  user.save(function (err){
    if(err){ 

      return next(err); }
      return res.json({token: user.generateJWT()})
    });
});


router.post('/login', function(req, res, next){


  if(!req.body.username || !req.body.password){
    return res.status(400).json({message: 'Please fill out all fields'});
  }

  passport.authenticate('local', function(err, user, info){
    if(err){ return next(err); }

    if(user){
      return res.json({token: user.generateJWT()});
    } else {
      return res.status(401).json(info);
    }
  })(req, res, next);
});







/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});



module.exports = router;
