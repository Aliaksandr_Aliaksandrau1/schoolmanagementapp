(function(){
  "use strict";

  angular
    .module("app")
    .config(['$stateProvider', '$urlRouterProvider', stateConfig]);


function stateConfig ($stateProvider, $urlRouterProvider) {
  $stateProvider
  
  // Home

  .state('home', {
    url: '/home',
    templateUrl: '/views/home.html',
    controller: 'Home'

  })


  // Head Teacher

  .state('pupilsHT', {
    url: '/headteacher/pupils',
    templateUrl: '/views/head-teacher/pupils-ht.html',
    controller: 'PupilsHT'

  })

  .state('teachersHT', {
    url: '/headteacher/teachers',
    templateUrl: '/views/head-teacher/teachers-ht.html',
    
    resolve: {
         // A string value resolves to a service
                userService: 'userService',

                // A function value resolves to the return
                // value of the function
                teachers: function(userService){
                    return  userService.getAllTeachers();
                }
      },
      controller: 'TeachersHT'

  })

  .state('schedulesHT', {
    url: '/headteacher/schedules',
    templateUrl: '/views/head-teacher/schedules-ht.html',
    controller: 'SchedulesHT'

  })

  .state('schoolClassesHT', {
    url: '/headteacher/classes',
    templateUrl: '/views/head-teacher/school-classes-ht.html',
    controller: 'SchoolClassesHT'

  })


  .state('subjectsHT', {
    url: '/headteacher/subjects',
    templateUrl: '/views/head-teacher/subjects-ht.html',
    controller: 'SubjectsHT'

  })

  // Teacher

   .state('sceduleT', {
    url: '/teacher/scedule',
    templateUrl: '/views/teacher/scedule-t.html',
    controller: 'SceduleT'

  })

   .state('journalT', {
    url: '/teacher/journal',
    templateUrl: '/views/teacher/journal-t.html',
    controller: 'JournalT'

  })

   .state('schoolClassJournalT', {
    url: '/teacher/myClass/journal',
    templateUrl: '/views/teacher/school-class-journal.html',
    controller: 'SchoolClassJournalT'

  })

   .state('schoolClassSceduleT', {
    url: '/teacher/myClass/scedule',
    templateUrl: '/views/teacher/school-class-scedule.html',
    controller: 'SchoolClassSceduleT'

  })


  // Pupil

  .state('diaryP', {
    url: '/pupil/diary',
    templateUrl: '/views/pupil/diary-p.html',
    controller: 'DiaryP'
  })

  .state('sceduleP', {
    url: '/pupil/scedule',
    templateUrl: '/views/pupil/scedule-p.html',
    controller: 'SceduleP'

  })


  // Authentification

  .state('login', {
    url: '/login',
    templateUrl: '/views/login.html',
    controller: 'Authentification',
    onEnter: ['$state', 'authService', function($state, authService){
      if(authService.isLoggedIn()){
        $state.go('home');
      }
    }]
  })

  .state('register', {
    url: '/register',
    templateUrl: '/views/register.html',
    controller: 'Authentification',
    onEnter: ['$state', 'authService', function($state, authService){
      if(authService.isLoggedIn()){
        $state.go('home');
      }
    }]
  })


  $urlRouterProvider.otherwise('home');
}



})();




