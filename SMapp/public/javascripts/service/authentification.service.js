 (function(){
    "use strict";
    
    angular
    .module("app")
    .factory('authService', authentificationService);

    function authentificationService ($http, $window) {

 	// API
 	return {
 		
        currentUser : currentUser,
        getToken : getToken,
        isLoggedIn : isLoggedIn,
        logIn : logIn,
        logOut : logOut,
        register : register,
        saveToken : saveToken
    };


 	// IMPL
 	function currentUser () {
        if(isLoggedIn()){
            var token = getToken();
            var payload = JSON.parse($window.atob(token.split('.')[1]));

            return payload.username;
        }
    }

    function getToken () {
        return $window.localStorage['smapp-token'];
    }

    function isLoggedIn () {
        var token = getToken();
        if(token){
            var payload = JSON.parse($window.atob(token.split('.')[1]));

            return payload.exp > Date.now() / 1000;
        } else {
            return false;
        }
    }

    function logIn (user) {
        return $http.post('/login', user).success(function(data){
            saveToken(data.token);
        });
    }

    function logOut () {
        $window.localStorage.removeItem('smapp-token');
    }

    function register (user) {
        return $http.post('/register', user).success(function(data){
           saveToken(data.token);
       });
    }

    function saveToken (token) {
        $window.localStorage['smapp-token'] = token;
    }

}

})();