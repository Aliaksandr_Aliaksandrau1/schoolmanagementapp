(function(){
    "use strict";
    
    angular
    .module("app")
      .factory("subjectService", subjectService);


function subjectService ($http) {
 
 	// API
 	return {
 		
    
 		getAllSubjects : getAllSubjects

 		
 	};


 	// IMPL
 	function getAllSubjects (subjects) {

        return $http.get('/getAllSubjects').success(function(data) {
            	 angular.copy(data, subjects);
        });
    }
  
   

 }

 })();