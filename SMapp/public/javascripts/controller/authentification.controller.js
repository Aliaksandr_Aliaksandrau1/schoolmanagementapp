
(function(){
  
  "use strict";

  angular
  	.module("app")
  	.controller('Authentification', Authentification);





 function Authentification($scope, $state, authService) {

 	$scope.user = {};

 	$scope.register = function(){
 		authService.register($scope.user).error(function(error){
 			$scope.error = error;
 		}).then(function(){
 			$state.go('home');
 		});
 	};

 	$scope.logIn = function(){
 		authService.logIn($scope.user).error(function(error){
 			$scope.error = error;
 		}).then(function(){
 			$state.go('home');
 		});
 	};

 }

})();


