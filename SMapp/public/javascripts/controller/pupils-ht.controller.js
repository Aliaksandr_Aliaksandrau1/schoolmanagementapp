 (function(){
 	"use strict";
 	
 	angular
 	.module("app")
 	.controller("PupilsHT", PupilsHT);



 	function PupilsHT($scope, userService) {
 		
 		$scope.pupils = []; 
 		$scope.user={active:true};
 		
 		$scope.getAllPupils = userService.getAllPupils;
 		
 		$scope.addPupil = function(){
 			
 			userService.addPupil($scope.user, $scope.pupils).error(function(error){
 				$scope.error = error;
 			})

 		};

 	}

 })();

